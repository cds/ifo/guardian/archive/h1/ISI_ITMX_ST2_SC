# General H1 states file for sensor correction

# Would be nice if we could get this to work for both BSCs and HAMs

from guardian import GuardState, GuardStateDecorator
from isiguardianlib.sensor_correction.st2_states import *
from isiguardianlib.sensor_correction import const as sc_const
from isiguardianlib.sensor_correction import edges as sc_edges

from . import st2_const as const


default_state = 'SC_OFF'
#############################################
# States

class INIT(GuardState):
    """Check the current configuration and then go to that state.
    If it is in an unknown state, we need to figure out where are 
    should go.
    
    In the mean time maybe we should have this state just go to down?
    """
    def main(self):
        # FIXME: Check config
        engaged_banks = []
        for filt, dofs in const.SC_FM_confs['DEFAULT'].items():
            for dof, fm in dofs.items():
                if fm:
                    channel = sc_const.SC_FILTER_CHANNEL_NAME.format(deg_of_free=dof,
                                                            bank=filt)
                    lf_bank = LIGOFilter(sc_const.SC_FILTER_CHANNEL_NAME.format(
                            deg_of_free=dof,
                            bank=filt),
                                             ezca)
                    bank_gain = lf_bank.GAIN.get()
                    if bank_gain > 0:
                        if filt not in engaged_banks:
                            engaged_banks.append(filt)
        if not engaged_banks:
            log('No banks engaged, going to {}'.format(default_state))
            return default_state                    
        elif len(engaged_banks) == 1:
            bank = engaged_banks[0]
            log('{} bank engaged, going to CONFIG_{}'.format(bank, bank))
            return 'CONFIG_{}'.format(bank)
        else:
            # If there are too many engaged, go to down
            log('No configuration found, going to DOWN')
            return 'DOWN'


# FIXME: Make sure to make this make sense
class SC_OFF(GuardState):
    """

    """
    index = 10
    #def main(self):
        # FIXME: Need to check if the filters that are default are engaged, but allow for testing
        # Check that the filters that should be engaged actually are
        #if check_config_engaged(const.SC_FM_confs['DEFAULT']):
        #    return True
        #else:
        #    return 'TURNING_OFF_SC'
    def run(self):
        # No need to continuously check here, testing may be going on
        return True


# In the context of SC, this state should be more of a parking spot
# for when we need the node to not do anything.
class DOWN(GuardState):
    index = 5
    def run(self):
        return True


turn_off_banks = [bank for bank in const.SC_FM_confs['DEFAULT']]
TURNING_OFF_SC = gen_sc_all_gains_off(turn_off_banks,\
                                      [dof for dof, fm in\
                                       # Assumes consistent dofs and only for dofs with fms in the list
                                       const.SC_FM_confs['DEFAULT'][turn_off_banks[0]].items() if fm] 
)

# Autogenerate the configuration states and edges
make_config_states(const.SC_FM_confs, method=2)
edges = sc_edges.make_st2_edges(const.SC_FM_confs, default_state_name=default_state)

edges += [('DOWN','TURNING_OFF_SC'),
          ('TURNING_OFF_SC', default_state),
          (default_state, 'DOWN'),
          ]
