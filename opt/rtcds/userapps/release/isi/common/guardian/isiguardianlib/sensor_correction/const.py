# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-
# SVN $Id$
# $HeadURL$
from .. import const as top_const

DEFAULT_RAMP_TIME = 5
SC_GAIN = 1
OUTPUT_SETTLE_TIME = 100

SC_DOF_LIST = dict(
        BSC_ST1 = ['X','Y','Z','RX','RY','RZ'],
        BSC_ST2 = ['X','Y','Z','RX','RY','RZ'],
        HAM = ['X','Y','Z','RX','RY','RZ'],
        )

SC_FILTER_BANK_NAMES_ALL = dict(
        BSC_ST1 = ['MATCH', 'FIR', 'IIRHP', 'WNR'],
        BSC_ST2 = ['MATCH', 'FIR', 'IIRHP', 'WNR'],
        HAM = ['MATCH', 'FIR', 'IIRHP', 'WNR'],
        HPI = ['MATCH', 'FIR', 'IIRHP', 'WNR']
        )

SC_FILTER_CHANNEL_NAMES_ALL = dict(
        BSC_ST1 = 'SENSCOR_GND_STS_{deg_of_free}_{bank}',
        HAM = 'SENSCOR_GND_STS_{deg_of_free}_{bank}',
   
        BSC_ST2 = 'SENSCOR_{deg_of_free}_{bank}',
        HPI = 'SENSCOR_{deg_of_free}_{bank}'
        )
 
# These are not needed yet, but may be in the future?
SC_ORIGIN_NAMES = dict(
        BSC_ST1 = ['GND_STS', 'HPI_L4C'],
        HAM = ['GND_STS','L4C']
        )


SC_FILTER_BANKS = SC_FILTER_BANK_NAMES_ALL[top_const.CHAMBER_TYPE]
SC_FILTER_CHANNEL_NAME = SC_FILTER_CHANNEL_NAMES_ALL[top_const.CHAMBER_TYPE]
